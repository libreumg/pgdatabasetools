# pgdatabasetools

<!-- badges: start -->
<!-- badges: end -->

The goal of pgdatabasetools is to ...

## Installation

You can install the released version of pgdatabasetools from [CRAN](https://CRAN.R-project.org) with:

``` r
install.packages("pgdatabasetools")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(pgdatabasetools)
## basic example code
```

