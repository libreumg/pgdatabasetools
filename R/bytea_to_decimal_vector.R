#' convert bytea to decimal vector
#'
#' @param raw_var the bytea object as it comes from a database request
#'
#' @return a decimal vector of the bytea object
#' @export
#' @author Stephan Struckmann
#'
#' @examples
#' \dontrun{
#'   dec <- bytea_to_decimal_vector("\\x746869732069732063726170")
#'   print(intToUtf8(dec))
#' }
#' 
bytea_to_decimal_vector <- function(raw_var) {
  raw <- raw_var
  
  # postgreSQL bytea fields are read with a trailing \\x, so remove this
  if (startsWith(raw_var, "\\x")) {
    raw <- substr(raw_var, start = 3, stop = nchar(raw_var))
  }
  
  splitted <- strsplit(raw, "", fixed = TRUE, useBytes = TRUE)[[1]]
  hex <- unlist(lapply(0:(-1 + nchar(raw) / 2), collapse_pairs, splitted))
  dec <- strtoi(hex, base = 16)
  
  return(dec)
}
